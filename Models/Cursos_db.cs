﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace apiGestores.Models
{
    public class Cursos_db
    {
        [Key]
        public int id { get; set; }

        [Required]
        [Display(Name = "Nombre del curso")]
        public String name { get; set; }

        [Display(Name = "Lanzamiento")]
        public int launch { get; set; }

        [Display(Name = "Desarrolador")]
        public String developer { get; set; }

    }
}
