using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using apiGestores.Context;
using apiGestores.Models;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;

namespace apiGestores.Pages.listCourses
{
    public class EditModel : PageModel
    {
        private readonly AppDbContext _db;
        public EditModel(AppDbContext db)
        {
            _db = db;
        }

        [BindProperty]
        public Cursos_db Curso { get; set; }

        [TempData]
        public string Mensaje { get; set; }

        public async Task OnGet(int id)
        {
            Curso = await _db.cursos_db.FindAsync(id);
        }

        public async Task<IActionResult> OnPost()
        {
            if (ModelState.IsValid)
            {
                var cursoDesdeDb = await _db.cursos_db.FindAsync(Curso.id);
                cursoDesdeDb.name = Curso.name;
                cursoDesdeDb.launch = Curso.launch;
                cursoDesdeDb.developer = Curso.developer;

                await _db.SaveChangesAsync();

                Mensaje = "Curso actualizado correctamente";

                return RedirectToPage("Index");
            }

            return RedirectToPage();
        }
    }
}
