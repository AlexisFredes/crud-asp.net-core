using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using apiGestores.Context;
using apiGestores.Models;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using Microsoft.EntityFrameworkCore;

namespace apiGestores.Pages.listCourses
{
    public class IndexModel : PageModel
    {
        private readonly AppDbContext _db;
        public IndexModel(AppDbContext db)
        {
            _db = db;
        }

        public IEnumerable<Cursos_db> Cursos { get; set; }

        [TempData]
        public string Mensaje { get; set; }

        public async Task OnGet()
        {
            Cursos = await _db.cursos_db.ToListAsync();
        }

        public async Task<IActionResult> OnPostDelete(int id)
        {
            var curso = await _db.cursos_db.FindAsync(id);

            if (curso == null)
            {
                return NotFound();
            }

            _db.cursos_db.Remove(curso);
            await _db.SaveChangesAsync();

            Mensaje = "Curso borrado correctamente";

            return RedirectToPage("index");
        }
    }
}
